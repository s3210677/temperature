package nl.utwente.di.translator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTranslator {

    @Test
    public void testBook1() throws Exception {
        Translator translator = new Translator();
        double degrees = translator.translator(30);
        Assertions.assertEquals(86.0, degrees, 0.0,"degrees in fahrenheit");
    }
}
