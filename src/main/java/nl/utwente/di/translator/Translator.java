package nl.utwente.di.translator;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Translator extends HttpServlet {

  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
          throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Celsius to Fahrenheit translator";

    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
            "<HTML>\n" +
            "<HEAD><TITLE>" + title + "</TITLE>" +
            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
            "</HEAD>\n" +
            "<BODY BGCOLOR=\"#FDF5E6\">\n" +
            "<H1>" + title + "</H1>\n" +
            "  <P>degrees Celsius: " +
            request.getParameter("celsius") + "\n" +
            "  <P>degrees Fahrenheit: " +
            translator(Double.parseDouble(request.getParameter("celsius"))) +
            "</BODY></HTML>");
  }

  public double translator(double celsius){
      return celsius * 9/5 + 32;
  }
}
